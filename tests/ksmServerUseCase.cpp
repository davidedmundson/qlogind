#include <QCoreApplication>
#include <QDebug>

#include "session.h"
#include "manager.h"

#include <QDBusConnection>

/**
 * Goals:
 *  - we need to load the session object for our session from PID
 *  - then monitor for lock/unlock signals
 *
 */

class FakeKSMServer: public QObject
{
    Q_OBJECT
public:
    FakeKSMServer();

public Q_SLOTS:
    void lock();
    void unlock();

private:
    SessionPtr m_session;
};


FakeKSMServer::FakeKSMServer(): QObject()
{
    PendingSession* ps = Session::sessionFromPid(QCoreApplication::applicationPid());

    QObject::connect(ps, &PendingSession::finished, [=](){
        m_session = ps->interface(); //this is required for memory management.

        connect(m_session.data(), &OrgFreedesktopLogin1SessionInterface::Unlock, this, &FakeKSMServer::unlock);
        connect(m_session.data(), &OrgFreedesktopLogin1SessionInterface::Lock, this, &FakeKSMServer::lock);
    });
}

void FakeKSMServer::lock()
{
    qDebug() << "LOCK";
}

void FakeKSMServer::unlock()
{
    qDebug() << "UNLOCK";
}


int main(int argc, char ** argv) {
    QCoreApplication app(argc, argv);
    FakeKSMServer f;

    app.exec();
}

#include "ksmServerUseCase.moc"
