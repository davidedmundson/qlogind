#include <QCoreApplication>
#include <QDebug>

#include "seat.h"
#include "seattracker.h"

#include <QDBusConnection>

/**
 * Goals:
 *  - keep track of all new seats which have CanGraphical
 *  - Seats can be added/removed at runtime
 *  - CanGraphical can change on/off at runtime
 *
 * This code is deliberately not using a lot of lambdas to check the API is compatiable with classic code styles
 * The code would be a lot smaller if we did
 */

class FakeSDDM: public QObject
{
    Q_OBJECT
public:
    FakeSDDM();

private Q_SLOTS:
    void onInitialSeatsFetched(PendingInterfaceInternal*);
    void onNewSeat(const SeatPtr &seat);
    void onGraphicalChanged();
    void onNewGraphical(const SeatPtr &seat);

private:
    QHash<QString, SeatPtr> m_seats;
    QStringList m_graphicalSeats;
};

FakeSDDM::FakeSDDM(): QObject()
{
    SeatTracker *seatTracker = new SeatTracker(this);
    PendingSeats *initialSeats = seatTracker->listSeats();
    connect(initialSeats, SIGNAL(finished(PendingInterfaceInternal*)), this, SLOT(onInitialSeatsFetched(PendingInterfaceInternal*)));

    connect(seatTracker, &SeatTracker::seatAdded, this, &FakeSDDM::onNewSeat);
//     connect(seatTracker, &OrgFreedesktopLogin1ManagerInterface::SeatRemoved, this, &FakeSDDM::onSeatRemoved);
}


void FakeSDDM::onInitialSeatsFetched(PendingInterfaceInternal *foo)
{
    auto x = static_cast<PendingSeats*>(foo);
    foreach(SeatPtr seat, x->interfaces()) {
        onNewSeat(seat);
    }
}

void FakeSDDM::onNewSeat(const SeatPtr &seat)
{
    qDebug() << "ASDF";
    qDebug() << seat->id();

    m_seats.insert(seat->id(), seat);

    if (seat->canGraphical()) {
        onNewGraphical(seat);
    }
    connect(seat.data(), SIGNAL(canGraphicalChanged()), this, SLOT(onGraphicalChanged()));
}

void FakeSDDM::onGraphicalChanged()
{
    Seat* seat = qobject_cast<Seat*>(sender());
    if (seat->canGraphical()) {
        onNewGraphical(QSharedPointer<Seat>(seat));
    } else {
        m_graphicalSeats.removeAll(seat->id());
        qDebug() << "graphical removed from " << seat->id();
    }
}

void FakeSDDM::onNewGraphical(const SeatPtr& seat)
{
    m_graphicalSeats.append(seat->id());
    qDebug() << "NEW GRAPHICAL FOR " << seat->id();
}

int main(int argc, char ** argv) {
    registerTypes();

    QCoreApplication app(argc, argv);
    FakeSDDM s;


    app.exec();
}

#include "sddmUseCase.moc"
