#include <QCoreApplication>
#include <QDebug>

#include "sessiontracker.h"

#include <QDBusConnection>

/**
 * Goals:
 *  - keep track of all sessions
 */

class SessionTrackTest: public QObject
{
    Q_OBJECT
public:
    SessionTrackTest();

private Q_SLOTS:
    void newSession(const SessionPtr &session);

private:
    QHash<QString, SessionPtr> m_sessions;
};

SessionTrackTest::SessionTrackTest(): QObject()
{
    SessionTracker *st = new SessionTracker(this);
    PendingSessions* ps = st->listSessions();
    connect(ps, &PendingInterfaceInternal::finished, this, [=](){
        foreach(const SessionPtr &session, ps->interfaces()) {
            newSession(session);
        }
    });

    connect(st, &SessionTracker::sessionAdded, this, &SessionTrackTest::newSession);
}

void SessionTrackTest::newSession(const SessionPtr& session)
{
    m_sessions.insert(session->id(), session);
    qDebug() << "NEW SESSION " << session->name();
}

int main(int argc, char ** argv) {
    registerTypes();

    QCoreApplication app(argc, argv);
    SessionTrackTest s;

    app.exec();
}

#include "sessionTracking.moc"
